import React from "react";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ChakraProvider} from "@chakra-ui/react"


import theme from './theme'
import MainLayout from "./layouts/MainLayout";
import FetchInitialData from './FetchInitialData';
import Season from "./pages/Season";
import Stations from './pages/Stations';

import "./App.css";
import "typeface-roboto";

const AppRoute = ({ component: Component, layout: Layout, title, ...rest }) => (
  <Route {...rest} render={props => (
    <Layout title={title}>
      <Component {...props} />
    </Layout>
  )} />
)

function App() {
  return (
    <ChakraProvider theme={theme}>
      {/* <ColorModeProvider> */}
        <Router>
          <FetchInitialData />
            <Switch>
              <AppRoute path="/stations" component={Stations} layout={MainLayout} title="Stations" />
              <AppRoute path="/" component={Season} layout={MainLayout} title="HSL City Bike Season 2019" />
            </Switch>
        </Router>
      {/* </ColorModeProvider> */}
    </ChakraProvider>
  );
}

export default App;
