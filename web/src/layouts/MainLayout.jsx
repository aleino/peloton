import React from 'react';
import { Flex, Box } from '@chakra-ui/react';
import Sidebar from '../components/Sidebar';
import Header from '../components/Header';

const MainLayout = ({ title, children }) => {
  return (
    <Flex>
      <Sidebar />
      <Box sx={{ width: '100%' }}>
        <Header title={title} />
        {children}
      </Box>
    </Flex>
  );
};

export default MainLayout;
