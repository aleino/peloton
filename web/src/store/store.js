import { action, createStore, thunk, computed } from 'easy-peasy';

import API from '../api/api';

const seasonModel = {
  totalTrips: 0,
  dailyTrips: [
    {
      date: '2019-08-28',
      count: 25732,
    },
    {
      date: '2019-08-15T',
      count: 25468,
    },
    {
      date: '2019-08-08T',
      count: 25293,
    },
  ],
  updateSeason: action((state, payload) => {
    return payload;
  }),
  fetchSeason: thunk(async (actions) => {
    const season = await API.fetchSeason();
    actions.updateSeason(season);
  }),
};

// STATION MODEL
const stationModel = {
  byId: {},
  selected: {
    departureStationId: '1',
    returnStationId: '2',
  },
  getSelectedStation: computed(
    (state) => state.byId[state.selected.departureStationId]
  ),
  list: computed((state) => Object.values(state.byId)),
  selectStation: action((state, payload) => {
    state.selected.departureStationId = payload;
  }),
  updateStations: action((state, payload) => {
    state.byId = payload;
  }),
  fetchStations: thunk(async (actions) => {
    const stations = await API.fetchStations();
    actions.updateStations(stations);
  }),
  updateStation: action((state, payload) => {
    state.byId[payload.id] = {
      ...state.byId[payload.id],
      ...payload.data,
    };
  }),
  fetchStation: thunk(async (actions, id) => {
    if (!id) {
      return null;
    }
    const data = await API.fetchStation(id);
    const payload = {
      id,
      data,
    };
    actions.updateStation(payload);
  }),
};

const storeModel = {
  season: seasonModel,
  stations: stationModel,
};

const store = createStore(storeModel);

export default store;
