import React from 'react';

import { useStoreState, useStoreActions } from 'easy-peasy';

import { Flex, InputGroup } from '@chakra-ui/react';

import {
  AutoComplete,
  AutoCompleteInput,
  AutoCompleteItem,
  AutoCompleteList,
} from '@choc-ui/chakra-autocomplete';

const StationSelector = () => {
  const [inputValue, setInputValue] = React.useState();

  const stations = useStoreState((state) => state.stations.list);
  const selectStation = useStoreActions(
    (actions) => actions.stations.selectStation
  );

  const handleSelect = (value) => {
    selectStation(stations.filter((station) => station.name === value)[0].id);
    setInputValue('');
  };

  const orderedStations = stations.sort((a, b) => a.name.localeCompare(b.name));

  return (
    <Flex p={3} mb={4} justifyContent="center" alignItems="center">
      <AutoComplete
        autofocus
        emphasize
        listAllValuesOnFocus
        openOnFocus
        rollNavigation
        selectOnFocus
        bg="white"
        onChange={(val) => handleSelect(val)}
      >
        <>
          <InputGroup>
            <AutoCompleteInput
              variant="filled"
              placeholder="Search Stations..."
              w="lg"
              bg="white"
              borderRadius={4}
              value={inputValue}
              onChange={(e) => setInputValue(e.target.value)}
            />
          </InputGroup>
          <AutoCompleteList w="lg">
            {orderedStations.map((station, oid) => (
              <AutoCompleteItem
                key={`option-${oid}`}
                value={station.name}
                textTransform="capitalize"
                align="center"
                w="lg"
              >
                {station.name} ({station.id})
              </AutoCompleteItem>
            ))}
          </AutoCompleteList>
        </>
      </AutoComplete>
    </Flex>
  );
};

export default StationSelector;
