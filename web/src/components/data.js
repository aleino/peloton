const data = [{
    week: 1,
    day: 1,
    value: 6,
  },
  {
    week: 1,
    day: 2,
    value: 7,
  },
  {
    week: 1,
    day: 3,
    value: 9,
  },
  {
    week: 1,
    day: 4,
    value: 11,
  },
  {
    week: 1,
    day: 5,
    value: 2,
  },
  {
    week: 1,
    day: 6,
    value: 9,
  },
  {
    week: 1,
    day: 7,
    value: 12,
  },
  {
    week: 2,
    day: 1,
    value: 7,
  },
  {
    week: 2,
    day: 2,
    value: 9,
  },
  {
    week: 2,
    day: 3,
    value: 2,
  },
  {
    week: 2,
    day: 4,
    value: 8,
  },
  {
    week: 2,
    day: 5,
    value: 4,
  },
  {
    week: 2,
    day: 6,
    value: 5,
  },
  {
    week: 2,
    day: 7,
    value: 6,
  },
];

export default data
