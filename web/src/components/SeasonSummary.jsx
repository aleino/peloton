import React from 'react';
import { Box, Flex, Text } from '@chakra-ui/react';

import Paper from './ui/Paper';

const StatCard = ({ value, title, color }) => (
  <Flex
    sx={{
      flexGrow: 1,
      margin: 2,
      width: '100%',
      // backgroundColor: '#ddd',s
    }}
  >
    <Paper grow>
      <Flex
        sx={{
          display: 'flex',
          flexDirection: 'column',
          flexGrow: 1,
          alignItems: 'center',
          padding: 4,
        }}
      >
        <Text
          color={color}
          sx={{
            fontSize: '4xl',
            weight: 'bold',
            marginBottom: 2,
          }}
        >
          {value}
        </Text>
        <Text>{title}</Text>
      </Flex>
    </Paper>
  </Flex>
);

const SeasonSummary = () => {
  const seasonStats = [
    {
      value: 107,
      title: 'Days',
      color: '#29D850',
    },
    {
      value: 378,
      title: 'Stations',
      color: '#29D850',
    },
    {
      value: '3 835 939',
      title: 'Trips',
      color: '#B529D8',
    },
    {
      value: '37 468',
      title: 'Happy Riders',
      color: '#B529D8',
    },
  ];
  return (
    <Box mb={4}>
      {/* <Heading>Season Summary</Heading> */}
      <Flex
        sx={{
          direction: 'row',
          justifyContent: 'space-between',
        }}
      >
        {seasonStats.map((stat) => (
          <StatCard
            key={stat.title}
            value={stat.value}
            title={stat.title}
            color={stat.color}
          />
        ))}
      </Flex>
    </Box>
  );
};
export default SeasonSummary;
