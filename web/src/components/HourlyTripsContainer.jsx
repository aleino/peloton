import React, { useEffect, useState } from 'react';
import { useCubeQuery } from '@cubejs-client/react';
import { useStoreState } from 'easy-peasy';

import HourlyTripsChart from './charts/HourlyTripsChart';

const HourlyTripsContainer = () => {
  const [data, setData] = useState([]);
  const departureStationId = useStoreState(
    (state) => state.stations.selected.departureStationId
  );

  const filters = [
    {
      member: 'Trips.departureStationId',
      operator: 'equals',
      values: [departureStationId.toString()],
    },
  ];

  const options = {
    resetResultSetOnChange: false,
  };

  const { resultSet, isLoading, error } = useCubeQuery(
    {
      measures: ['Trips.count'],
      timeDimensions: [],
      dimensions: ['Trips.DepartureWeekHour'],
      order: {
        'Trips.count': 'desc',
      },
      filters,
    },
    options
  );

  useEffect(() => {
    if (!resultSet?.series()[0]?.series) {
      return null;
    }
    setData(
      resultSet.series()[0].series.map((obj) => ({
        value: obj.value,
        day: Number(obj.x.split('-')[0]),
        hour: Number(obj.x.split('-')[1]),
      }))
    );
  }, [resultSet]);

  if (error) {
    return <div>{error.toString()}</div>;
  }

  if (!(resultSet && resultSet.series())) {
    return null;
  }

  return (
    <div style={{ marginBottom: '3em' }}>
      <HourlyTripsChart data={data} isLoading={isLoading} />
    </div>
  );
};

export default HourlyTripsContainer;
