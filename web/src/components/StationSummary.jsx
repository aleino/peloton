import React from 'react';

import { useStoreState } from 'easy-peasy';
import get from 'lodash/get';

const StationSummary = () => {
  const station = useStoreState((state) => state.stations.getSelectedStation);
  const totalTrips = get(station, 'totalTrips', 0);

  return <div>Total trips {totalTrips}</div>;
};

export default StationSummary;
