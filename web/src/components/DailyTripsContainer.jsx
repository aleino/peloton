import React from 'react';
import { useCubeQuery } from '@cubejs-client/react';
import { Box } from '@chakra-ui/react';

import DailyTripsChart from './charts/DailyTripsChart';

const DailyTripsContainer = ({ stations = [] }) => {
  // DEFINE FILTERS
  let filters = [];

  if (stations.length) {
    const filter = {
      member: 'Trips.departureStationId',
      operator: 'equals',
      values: stations.map((s) => s.toString()),
    };
    filters.push(filter);
  }

  // QUERY
  const { resultSet, isLoading, error, progress } = useCubeQuery({
    measures: ['Trips.count'],
    timeDimensions: [
      {
        dimension: 'Trips.departureTime',
        granularity: 'day',
      },
    ],
    order: {},
    filters,
  });

  if (isLoading) {
    return (
      <div>
        {(progress && progress.stage && progress.stage.stage) || 'Loading...'}
      </div>
    );
  }

  if (error) {
    return <div>{error.toString()}</div>;
  }

  if (!(resultSet && resultSet.series())) {
    return null;
  }

  const series = resultSet.series();
  return (
    series.length && (
      <Box sx={{ marginBottom: 4 }}>
        <DailyTripsChart data={series[0]['series']} />
      </Box>
    )
  );
};

export default DailyTripsContainer;
