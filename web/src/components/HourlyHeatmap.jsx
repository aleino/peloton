import React, { Component } from 'react';

import * as d3 from 'd3';
import data from './data';

class HourlyHeatmap extends Component {
  drawChart() {
    const svgWidth = 500;
    const svgHeight = 500;
    const colorDomain = d3.extent(data, (d) => d.value);

    const colorScale = d3
      .scaleSequential()
      .domain(colorDomain)
      .interpolator(d3.interpolateViridis);

    const svg = d3
      .select('#heatmap')
      .append('svg')
      .attr('width', svgWidth)
      .attr('height', svgHeight);

    const rectangles = svg.selectAll('rect').data(data).enter().append('rect');

    rectangles
      .attr('x', (d) => d.day * 50)
      .attr('y', (d) => d.week * 50)
      .attr('width', 50)
      .attr('height', 50)
      .style('fill', (d) => colorScale(d.value));
  }

  componentDidMount() {
    this.drawChart();
  }

  render() {
    return (
      <div>
        <div id="heatmap"></div>
      </div>
    );
  }
}

export default HourlyHeatmap;
