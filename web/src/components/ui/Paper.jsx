import React from 'react';
import { Flex } from '@chakra-ui/react';

const Paper = ({ grow, children }) => {
  return (
    <Flex
      sx={{
        backgroundColor: '#fff',
        borderRadius: '8px',
        border: '1px #eee solid',
        flexGrow: grow ? '1' : null,
      }}
    >
      {children}
    </Flex>
  );
};

export default Paper;
