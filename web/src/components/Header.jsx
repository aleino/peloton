import React from 'react';
import { Flex, Box, Heading, IconButton } from '@chakra-ui/react';
import { Filter } from 'react-feather';

const Header = ({ title }) => {
  return (
    <Flex
      sx={{
        flex: '1 100%',
        height: '72px',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomWidth: '1px',
        borderColor: 'gray.200',
        borderBottomStyle: 'solid',
        padding: 4,
        paddingLeft: 8,
        marginBottom: 4,
      }}
    >
      <Heading size="lg">{title}</Heading>
      <Box>
        <IconButton>
          <Filter />
        </IconButton>
      </Box>
    </Flex>
  );
};

export default Header;
