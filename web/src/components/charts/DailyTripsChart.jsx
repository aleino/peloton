import React from 'react';
import {
  CartesianGrid,
  Cell,
  XAxis,
  YAxis,
  Tooltip,
  ResponsiveContainer,
  Legend,
  BarChart,
  Bar,
} from 'recharts';
import moment from 'moment';
import { interpolateViridis } from 'd3-scale-chromatic';

const DailyTripsChart = ({ data }) => {
  const maxCount = Math.max(...data.map((d) => d['value']));
  const minCount = Math.min(...data.map((d) => d['value']));

  const scaleColor = (count) => {
    const scalar = 1 - count / (maxCount - minCount);
    return interpolateViridis(scalar);
  };

  return data ? (
    <ResponsiveContainer width="100%" height={350}>
      <BarChart data={data}>
        <XAxis
          dataKey="x"
          tickFormatter={(str) => moment(str).format('D.M.')}
        />
        <YAxis />
        <CartesianGrid />
        <Bar dataKey="value" name="Daily Trips">
          {data.map((d) => (
            <Cell key={d.name} fill={scaleColor(d['value'])}></Cell>
          ))}
        </Bar>
        <Legend />
        <Tooltip />
      </BarChart>
    </ResponsiveContainer>
  ) : (
    <ResponsiveContainer width="100%" height={350}></ResponsiveContainer>
  );
};

export default DailyTripsChart;
