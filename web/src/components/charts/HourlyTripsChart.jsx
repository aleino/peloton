import React, { useEffect, useRef } from 'react';

import * as d3 from 'd3';

// import { getGaussianRandom } from '../../utils/random';
import { interpolateViridis } from 'd3-scale-chromatic';

const TRANSITION_DURATION = 150;


const HourlyTripsChart = ({ data, isLoading }) => {
  const ref = useRef();

  const tileHeight = 40;
  const tileWidth = 50;

  const svgWidth = 24 * tileWidth;
  const svgHeight = 7 * tileHeight;

  const svg = d3.select(ref.current);

  const colorDomain = d3.extent(data, (d) => d.value).reverse();
  const colorScale = d3
    .scaleSequential()
    .domain(colorDomain)
    .interpolator(interpolateViridis);

  // prettier-ignore
  svg
  .attr('width', svgWidth)
  .attr('height', svgHeight);

  useEffect(() => {
    svg
      .selectAll('rect')
      .data(data)
      .join(
        (enter) => enter.append('rect')
        // (update) => update.attr('class', 'updated-elem')
      )
      .attr('x', (d) => d.hour * tileWidth)
      .attr('y', (d) => d.day * tileHeight)
      .attr('width', tileWidth)
      .attr('height', tileHeight)
      .transition()
      .duration(TRANSITION_DURATION)
      .style('fill', (d) => colorScale(d.value));
  }, [data, svg, colorScale]);

  return (
    <div className="hourly-trips">
      <div style={{ height: '40px' }}>{isLoading && 'Loading'}</div>
      <svg ref={ref} />
    </div>
  );
};

export default HourlyTripsChart;
