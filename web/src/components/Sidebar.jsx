import React from 'react';
import { Flex, Box, Text } from '@chakra-ui/react';
import * as Icon from 'react-feather';

import { NavLink } from 'react-router-dom';

const HeaderLogo = () => {
  return (
    <Box to="/" style={{ textDecoration: 'none' }}>
      <Flex
        mb={4}
        sx={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          height: '72px',
          backgroundColor: 'purple.400',
          color: 'white',
          fontWeight: 'bold',
          fontSize: 'md',
          fontFamily: 'monospace',
          borderBottomWidth: '1px',
          borderColor: 'gray.200',
          borderBottomStyle: 'solid',
        }}
      >
        peloton
      </Flex>
    </Box>
  );
};

const SidebarLink = ({ to, title, icon }) => (
  <NavLink
    className="nav_link" // Hacky: Check App.css
    exact
    as={NavLink}
    to={to}
    style={{
      textDecoration: 'none',
    }}
    activeStyle={{
      fontWeight: 'bold',
      color: '#2D29D8',
    }}
  >
    <Flex
      sx={{
        alignItems: 'center',
        padding: 4,
        paddingLeft: 4,
        '&:hover': {
          background: 'gray.200',
        },
      }}
    >
      {icon}
      <Text ml={2}>{title}</Text>
    </Flex>
  </NavLink>
);

function Sidebar() {
  return (
    <Flex
      className="sidebar"
      as="aside"
      w={48}
      sx={{
        flexDirection: 'column',
        // width: '192px',
        height: '100vh',
        borderRightWidth: '1px',
        borderColor: 'gray.200',
        borderRightStyle: 'solid',
      }}
    >
      <HeaderLogo />
      <nav>
        <SidebarLink to="/" title="Season" icon={<Icon.Calendar />} />
        <SidebarLink to="/stations" title="Stations" icon={<Icon.MapPin />} />
        <SidebarLink to="/trips" title="Trips" icon={<Icon.Play />} />
        <SidebarLink to="/anomalies" title="Anomalies" icon={<Icon.Zap />} />
        <SidebarLink to="/about" title="About" icon={<Icon.Info />} />
      </nav>
    </Flex>
  );
}

export default Sidebar;
