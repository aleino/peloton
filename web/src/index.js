import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

import { StoreProvider } from 'easy-peasy';
import store from './store/store';

import { CubeProvider } from '@cubejs-client/react';
import cubejsApi from './api/cubejsApi';

import 'normalize.css';

/* eslint-disable no-undef */
if (module.hot) {
  module.hot.accept();
}
/* eslint-disable no-undef */

ReactDOM.render(
  <React.StrictMode>
    <StoreProvider store={store}>
      <CubeProvider cubejsApi={cubejsApi}>
        <App />
      </CubeProvider>
    </StoreProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
