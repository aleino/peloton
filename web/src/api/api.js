// const API_BASE_URL = "http://localhost:5000/api";

const fetchStations = async () => {
  const seasonDataUrl = 'stations.json';
  const response = await fetch(seasonDataUrl);
  const json = await response.json();
  const stations = {};
  json.stations.forEach((s) => {
    stations[s.id] = s;
  });
  return stations;
};

const API = {
  fetchStations,
};

export default API;
