import cubejs from '@cubejs-client/core';

const API_URL = 'http://localhost:4000'; // change to your actual endpoint

const cubejsApi = cubejs(
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MDIwOTgwMzIsImV4cCI6MTYwNDY5MDAzMn0.W58LBRu_nzECOTkxK-RLoMhuqoEWmekqj3BV8O40960',
  { apiUrl: API_URL + '/cubejs-api/v1' }
);

export default cubejsApi;
