import { extendTheme } from '@chakra-ui/react';

const theme = extendTheme({
  config: {
    initialColorMode: 'light',
    useSystemColorMode: false,
  },
  colors: {
    background: '#fefefe',
  },
  styles: {
    global: {
      'html, body': {
        bg: 'gray.100',
      },
      a: {
        color: 'teal.500',
      },
    },
  },
  header: {
    height: '712px',
  },
});

export default theme;
