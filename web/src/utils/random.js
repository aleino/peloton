export function getGaussianRandom(iterations = 10) {
  let rand = 0;
  for (let i = 0; i < 4; i++) {
    rand += Math.random();
  }
  return rand / iterations;
}
