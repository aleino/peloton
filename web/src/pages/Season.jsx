import React from 'react';

import SeasonSummary from '../components/SeasonSummary';
import DailyTripsContainer from '../components/DailyTripsContainer';
import { Heading, Box } from '@chakra-ui/react';

const Season = () => {
  return (
    <Box maxW="container.xl" m="0 auto">
      <SeasonSummary />
      <Heading size="md" mb={4}>
        Daily Trips
      </Heading>
      <DailyTripsContainer />
    </Box>
  );
};

export default Season;
