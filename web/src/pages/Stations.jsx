import React from 'react';

import { useStoreState } from 'easy-peasy';

import StationSelector from '../components/StationSelector';
import DailyTripsContainer from '../components/DailyTripsContainer';
import HourlyTripsContainer from '../components/HourlyTripsContainer';

import { Box, Heading, Flex } from '@chakra-ui/react';

const Stations = () => {
  const selectedStationId = useStoreState(
    (state) => state.stations.selected.departureStationId
  );

  const selectedStation = useStoreState(
    (state) => state.stations.getSelectedStation
  );

  return (
    <Box maxW="container.xl" m="0 auto">
      <Flex w="full" justifyContent="center" alignItems="center">
        <StationSelector />
      </Flex>
      <Heading as="h1" size="xl" mb={4}>
        {selectedStation?.name}
      </Heading>
      <Heading size="md" mb={4}>
        Hourly Trips
      </Heading>
      <HourlyTripsContainer />
      <Heading size="md" mb={4}>
        Daily Trips
      </Heading>
      <DailyTripsContainer
        stations={selectedStationId ? [selectedStationId] : []}
      />
      {/* <StationSummary /> */}
    </Box>
  );
};

export default Stations;
