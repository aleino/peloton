import React, { useEffect, Fragment } from 'react';

import { useStoreActions } from 'easy-peasy';

const FetchInitialData = () => {
  const fetchStations = useStoreActions(
    (actions) => actions.stations.fetchStations
  );
  useEffect(() => {
    fetchStations();
  }, [fetchStations]);

  return <Fragment></Fragment>;
};

export default FetchInitialData;
