# Database Initialization Scripts

Scripts are designed to be run from the root (`server`) directory.

## Requirements

- wget
- Docker

## Fetch raw data

To fetch the raw trips data fom HSL run:

```sh
./scripts/init-db/fetch_raw_data.sh
```

This will fetch and unzip the zipped raw data file into `data/raw/` directory.

## Initialize Database

To fetch the data and to initialize the database run:

```sh
./scripts/init-db/init_db.sh
```
