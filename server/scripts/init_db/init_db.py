import glob
import os
import sqlite3

import pandas as pd
import numpy as np


DATA_DIR = os.path.join('/app/data')
RAW_DIR = os.path.join(DATA_DIR, 'raw')
TRIPS_DIR = os.path.join(RAW_DIR, 'od-trips-2019')
OUTPUT_DIR = os.path.join(DATA_DIR, 'output')

DB_DIR = '/app/db'
DB_PATH = os.path.join(DB_DIR, 'peloton.sqlite3')


def load_trips():
    print(DATA_DIR)
    print(RAW_DIR)
    print(TRIPS_DIR)
    csv_files = glob.glob(os.path.join(TRIPS_DIR, '*.csv'))
    names = [
        'departure_time',
        'return_time',
        'departure_station_id',
        'departure_station_name',
        'return_station_id',
        'return_station_name',
        'covered_distance',
        'duration'
    ]

    date_cols = [
        'departure_time',
        'return_time',
    ]

    dtype = {
        'departure_station_id': 'object',
        'departure_station_name': 'object',
        'return_station_id': 'object',
        'return_station_name': 'object',
    }

    for csv_file in csv_files:
        print(csv_file)
    df = pd.concat(pd.read_csv(f, names=names, header=0,
                               parse_dates=date_cols, dtype=dtype) for f in csv_files)

    return df


def save_db(trips):
    con = sqlite3.connect(DB_PATH)

    cols_to_drop = ['departure_station_name', 'return_station_name']
    for col in cols_to_drop:
        trips.drop(col, axis=1, inplace=True)

    dtypes = {
        'covered_distance': np.int
    }
    trips.to_sql(con=con, name='Trips', index_label='id', if_exists='replace')


if __name__ == "__main__":
    print('START Load trips')
    trips = load_trips()
    print('END Load trips')
    print('START Save trips')
    save_db(trips)
    print('END Save trips')
