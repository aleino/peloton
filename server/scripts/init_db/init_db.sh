#!/bin/bash
set -euxo pipefail

# Fetch raw data
scripts/init_db/fetch_raw_data.sh

# Build database initialization image
docker build --tag peloton-init-db scripts/init_db

# Run database initialization
docker run -it \
--mount type=bind,source="${PWD}/data",target=/app/data \
--mount type=bind,source="${PWD}/db",target=/app/db \
peloton-init-db
