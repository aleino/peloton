#!/bin/bash
set -euxo pipefail

TRIPS_RAW_FILENAME="od-trips-2019.zip"
TRIPS_RAW_ZIP_PATH="data/raw/${TRIPS_RAW_FILENAME}"
TRIPS_RAW_ZIP_URL="http://dev.hsl.fi/citybikes/od-trips-2019/od-trips-2019.zip"

mkdir -p data/{raw,output}
mkdir -p db

maybe_download_trips () {
if [ ! -f $TRIPS_RAW_ZIP_PATH ]; then
    wget $TRIPS_RAW_ZIP_URL --output-document $TRIPS_RAW_ZIP_PATH
fi
}

unzip_trips () {
  cd data/raw
  unzip $TRIPS_RAW_FILENAME
}

maybe_download_trips
unzip_trips
