var express = require('express');
var router = express.Router();

const sequelize = require('sequelize')
const Trip = require('../models').Trip

const queryTotalTrips = (id) => (
  Trip.count({
    where: {
      departure_station_id: id
    }
  })
  .then((count) => count)
)

const queryDailyTrips = (id) => (
  Trip.findAll({
    where: {
      departure_station_id: id
    },
    attributes: [
      [sequelize.literal(`DATE("departure_time")`), 'date'],
      [sequelize.literal(`COUNT(*)`), 'count']
    ],
    group: ['date'],
  })
)

// GET Season summary
router.get('/:id', async function (req, res) {
  const {
    id
  } = req.params;
  const totalTrips = await queryTotalTrips(id)
  const dailyTrips = await queryDailyTrips(id)
  // const dailyTrips = await queryDailyTrips()
  res.json({
    'totalTrips': totalTrips,
    'dailyTrips': dailyTrips,
    // 'dailyTrips': dailyTrips
  })

})

module.exports = router;
