var express = require('express');
var router = express.Router();

const sequelize = require('sequelize')

const Trip = require('../models').Trip

const queryTotalTrips = () => (
  Trip.count({})
  .then((count) => count)
)

const queryDailyTrips = () => (
  Trip.findAll({
    attributes: [
      [sequelize.literal(`DATE("departure_time")`), 'date'],
      [sequelize.literal(`COUNT(*)`), 'count']
    ],
    group: ['date'],
  })
)

// GET Season summary
router.get('/', async function (req, res) {
  const totalTrips = await queryTotalTrips()
  const dailyTrips = await queryDailyTrips()
  res.json({
    'totalTrips': totalTrips,
    'dailyTrips': dailyTrips
  })

})

module.exports = router;
