var express = require('express');
var router = express.Router();


// Home page route.
router.get('/', function (req, res) {
  res.send('Hello');
})

router.get('/health', function (req, res) {
  res.json({
    'status': 'ok'
  });
})


module.exports = router;
