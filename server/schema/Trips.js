cube(`Trips`, {
  sql: `SELECT * FROM main."Trips"`,

  joins: {},

  measures: {
    count: {
      type: `count`,
      drillMembers: [id, departureStationId, returnStationId],
    },

    duration: {
      sql: `duration`,
      type: `avg`,
      title: `Average Duration (s)`,
      description: `Average trip duration`,
    },
  },

  dimensions: {
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true,
    },

    DepartureWeekHour: {
      sql: `strftime("%w-%H", "departure_time")`,
      type: `string`,
      shown: true,
      description: `Count trips for every hour in a week `,
    },

    departureStationId: {
      sql: `departure_station_id`,
      type: `string`,
    },

    returnStationId: {
      sql: `return_station_id`,
      type: `string`,
    },

    departureTime: {
      sql: `departure_time`,
      type: `time`,
    },

    returnTime: {
      sql: `return_time`,
      type: `time`,
    },
  },
});
